package TestTDD;

import java.util.HashMap;
import java.util.Map;

import CashMachine.ContaCorrente;
import CashMachine.ServicoRemoto;

public class mockServicoRemoto implements ServicoRemoto {
	Map<Integer, Integer> saldoUsuario = new HashMap<>();
	
	public mockServicoRemoto(){
		this.saldoUsuario.put(10000000, 1000);
	}
	
	@Override
	public int getSenha(int numeroCartaoConta){
		if(numeroCartaoConta==10000000)
			return 1111;
		else
			return 0;
	}

	@Override
	public int recuperarConta(int conta) {
		return this.saldoUsuario.get(conta);
	}

	@Override
	public int persistirConta(int valor, int conta) {
		int saldo = this.saldoUsuario.get(conta);
		saldo=saldo+valor;
		this.saldoUsuario.put(conta, saldo);
		return saldo;
	}

}
