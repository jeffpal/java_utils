package TestTDD;
import static org.junit.Assert.*;

import org.junit.Test;

import CashMachine.CaixaEletronico;
import CashMachine.ServicoRemoto;
import CashMachine.mockHardware;

public class TestCaixaEletronico {

	@Test
	public void testLogar() throws contaInesistenteException {
		int conta, senha;
		CaixaEletronico usuarioX = new CaixaEletronico();
		
		mockHardware cartao1 = new mockHardware();
		conta = cartao1.pegarNumeroDaContaCartao();
		senha = cartao1.pegarSenha();
		
		mockServicoRemoto operacao1 = new mockServicoRemoto();
		
		assertEquals(1111, usuarioX.logar(conta, senha, operacao1));
	}
	
	@Test
	public void testSacar() {
		int conta = 10000000, valor=700;
		CaixaEletronico usuarioX = new CaixaEletronico();
		mockHardware cartao1 = new mockHardware();
		mockServicoRemoto operacao1 = new mockServicoRemoto();
		assertEquals("Retire seu dinheiro. Saldo atual = 300", usuarioX.sacar(valor, conta, operacao1, cartao1));
	}
	
	@Test
	public void testSaldo() {
		int conta = 10000000;
		CaixaEletronico usuarioX = new CaixaEletronico();
		mockServicoRemoto operacao1 = new mockServicoRemoto();
		assertEquals("O saldo é R$ 1000", usuarioX.saldo(conta, operacao1));
	}

	@Test
	public void testDeposito() {
		int conta = 10000000, valor = 240;
				
		CaixaEletronico usuarioX = new CaixaEletronico();
		mockHardware cartao1 = new mockHardware();
		cartao1.lerEnvelope(conta, valor);
		
		mockServicoRemoto operacao1 = new mockServicoRemoto();
		
		assertEquals("Depósito recebido com sucesso. Depositado: 1240", usuarioX.deposito(conta, valor, operacao1));
	}
}
