package CashMachine;

public interface Hardware {
	public int pegarNumeroDaContaCartao();
	public int pegarSenha();
	public void entregarDinheiro();
	public void lerEnvelope(int conta, int valor);
	
}
