package CashMachine;

import TestTDD.contaInesistenteException;
import TestTDD.mockServicoRemoto;

public class CaixaEletronico {
	int numeroCartaoConta, senha, saldo;
		
	
	public int logar(int numeroCartaoConta, int senha, ServicoRemoto servico) throws contaInesistenteException {
		this.numeroCartaoConta=numeroCartaoConta;
		this.senha=senha;
		
		
		if(senha==servico.getSenha(numeroCartaoConta))
			return senha;
		else
			System.err.println("Login inválido");
			throw new contaInesistenteException("Conta ou senha inválidos!");
	}
	
	public String sacar(int valor, int conta, ServicoRemoto servico, Hardware cartao1){
		
		if(valor > servico.recuperarConta(conta)){
			System.out.println("saldo insuficiente");
			return "Saldo insufuciente";
		}
		else{
			int saldo = servico.persistirConta(-valor, conta);
			cartao1.entregarDinheiro();
			String saldoMessage = "Retire seu dinheiro. Saldo atual = "+Integer.toString(saldo);
			return saldoMessage;
		}		
	}
	
	public String saldo(int conta, ServicoRemoto servico){
		String saldo="O saldo é R$ "+Integer.toString(servico.recuperarConta(conta));
		return (saldo);
	}
	
	public String deposito(int conta, int valor, ServicoRemoto servico){
		
		return "Depósito recebido com sucesso. Depositado: "+servico.persistirConta(valor, conta);
	}
}

