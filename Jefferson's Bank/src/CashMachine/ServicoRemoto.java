package CashMachine;

import TestTDD.contaInesistenteException;

public interface ServicoRemoto {
	
	public int getSenha(int numeroCartaoConta);
	
	public int recuperarConta(int conta);
	
	public int persistirConta(int valor, int conta);
}
