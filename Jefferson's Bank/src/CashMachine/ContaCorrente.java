package CashMachine;

import java.util.HashMap;
import java.util.Map;

import TestTDD.contaInesistenteException;

public class ContaCorrente {
	
	private int numeroConta, senha, saldo;
	
	public ContaCorrente(int setConta, int setSenha, int setSaldo){
		this.numeroConta = setConta;
		this.senha = setSenha;
		this.saldo = setSaldo;
	}
	
	public int getSenha(int conta){
		if(conta == numeroConta)
			return senha;
		else
			return 0;
	}

	public int getSaldo(){
		return saldo;
	}
}
