
public class JPrint {

	public void PrintlnAsciiTable() {
		char c;
		for(c=0; c<127; c++){
			System.out.println("Decimal: "+(int)c+" Ascii: "+(char)c);
			
		}
	}

	public void PrintfAsciiTable() {
		char c;
		for(c=0; c<127; c++){
		//I don't recommend to use concatenation into printf using variables with value type different of string
		System.out.printf("Decimal: %d Ascii: %c\n",(int)c, (char)c);
		}
	}
}
