import java.util.Scanner;

class Fibonacci{

    public static void main (String[] args){

        Scanner Entrada = new Scanner(System.in);

        System.out.println("Calcular o n-ésimo de Fibonnaci:  ");
        fibo(Entrada.nextInt());

        System.out.println("Calcular o n-ésimo de Fibonnaci:  ");
        System.out.println(fiboRecursive(Entrada.nextInt()));
    }

     static void fibo(int n_esimoFibo) {

        if(n_esimoFibo<=3)
            System.out.println("Informe um valor maior ou igual a 3");

        int fibo1=1, fibo2=1;
        for(int i=3; i<=n_esimoFibo; i++){

            fibo2=fibo2+fibo1;  //atualiza o valor de fibo2 para a soma dos atuais valores de fibo1 e fibo2
            fibo1=fibo2-fibo1;  //o novo valor de fibo1 passa a ser o antigo valor fe fibo2
            System.out.println(i+"-ésino número de Fionacci: "+fibo2);
            }

        }

        static long fiboRecursive(int n) {

        if (n < 2) {
            return n;
            }
        else {
            return fiboRecursive(n - 1) + fiboRecursive(n - 2);
            }

        }

}