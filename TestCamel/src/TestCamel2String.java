import static org.junit.Assert.*;
import org.junit.Test;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class TestCamel2String {

	@Test
	public void TestInitWithNumber() {
		Camel2String Camel = new Camel2String();
		assertFalse(Camel.InitWithNumber("CamelIniciandoComNumero"));
	}
	
	@Test
	public void TestSpecialCharacter() {
		Camel2String Camel = new Camel2String();
		assertFalse(Camel.SpecialCharacter("CamelComCaracterEspecial"));
	}
	
	@Test
	public void TestBreakCamel() {
		Camel2String Camel = new Camel2String();
		List<String> string = Camel.converterCamelCase("CamelCase");
		assertEquals(string.get(0), "Camel");
	}
}
