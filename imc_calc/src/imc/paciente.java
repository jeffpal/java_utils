package imc;

public class paciente {
	double peso, altura, IMC;
	public paciente(double peso, double altura){
		this.peso=peso;
		this.altura=altura;
	}
	
	public void calculaIMC(){
		IMC = peso/(altura*altura);
	}
	
	public String diagnostico(){
		String message=null;
		calculaIMC();
		if (IMC>=40)
			message="Obesidade grau III (obesidade mórbida) = IMC igual ou maior que 40 kg/m²";
		
		else if(IMC >=35 && IMC <=39.99)
			message="Obesidade grau II = IMC entre 35 e 39,99 kg/m²";
		
		else if(IMC >=30 && IMC <=34.99)
			message="Obesidade grau I = IMC entre 30 e 34,99 kg/m²";
			
		else if(IMC >=25 && IMC <=29.99)
			message="Sobrepeso = IMC entre 25 e 29,99 kg/m²";
					
		else if(IMC >=18.5 && IMC <=24.99)
			message="Peso normal = IMC entre 18,50 e 24,99 kg/m²";

		else if(IMC >=17 && IMC <=18.49)
			message="Baixo peso = IMC entre 17 e 18,49 kg/m²";
			
		else if(IMC >=16 && IMC <=16.99)
			message="Baixo peso grave = IMC entre 16 e 16,99 kg/m²";
			
		else if(IMC <16)
			message="Baixo peso muito grave = IMC abaixo de 16 kg/m²";
		
		return message;
	}	

}
